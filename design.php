<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageHeader/InsideSliderBanner.php') ?>

<!-- Sidebar Section -->
<section class="Section DesignPage">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="asideContainer">
                    <ul class="asideInner">
                        <li>
                            <a href="#Container-1" class="is_active">Design that is Iconic & Sublime</a>
                        </li>
                        <li>
                            <a href="#Container-2">Workwell</a>
                        </li>
                        <li>
                            <a href="#Container-3">Superior Construction Material</a>
                        </li>
                        <li>
                            <a href="#Container-4">Multifunctional Spaces</a>
                        </li>
                        <li>
                            <a href="#Container-5">Sustainability LEED Gold Certified</a>
                        </li>
                        <li>
                            <a href="#Container-6">The philosophy of biophilia</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="NormalSection PTop12px">
                    <div class="NormalSectionContainer" id="Container-1">
                        <h2 class="NormalHeading">Design that is Iconic & Sublime</h2>
                        <p>Designed meticulously to be highly functional while being aesthetically pleasing. The unique façade, made of terracotta bricks, showcases the rich industrial history of Okhla. The iconic clock tower on the South-West side of the building makes Max House an unmissable address.</p> 
                        <p>As a result, the workspace we offer our tenants acts as an area of advantage for retention, recruitment, development, and well-being. Hence, we wanted to re-think the workspace to not only be a real estate asset but a strategic asset for our tenants.</p>
                        <div class="imgWrap TwoImg">
                            <img src="assets/img/tempimg/iconic-sublime-1.png" alt="">
                            <img src="assets/img/tempimg/iconic-sublime-2.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-2">
                        <h2 class="NormalHeading">Workwell</h2>
                        <p>Designed meticulously to be highly functional while being aesthetically pleasing. The unique façade, made of terracotta bricks, showcases the rich industrial history of Okhla. The iconic clock tower on the South-West side of the building makes Max House an unmissable address.</p> 
                        <p>As a result, the workspace we offer our tenants acts as an area of advantage for retention, recruitment, development, and well-being. Hence, we wanted to re-think the workspace to not only be a real estate asset but a strategic asset for our tenants.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/workwell.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-3">
                        <h2 class="NormalHeading">Simply Superior Materials & Construction</h2>
                        <p>The materials used to build each space have been carefully chosen to maintain a sense of luxury and balancing it with our high sustainability design standards. From unique glass bricks to terracotta brick tiles, everything at Max House has been hand-picked to ensure a sense of luxury, comfort and longevity.</p> 
                        <p>The common areas of Max House are well appointed with: <br> A combination of brick and marble which exude a sense of welcome and warmth Double glazed windows to lower operating costs while allowing light transmission Wooden wall finish & panelling High ceilings with a height of 3.75 meters Efficient floor plates with a clean, efficient rectangular design Plants and nature wherever you look</p>
                        <div class="imgWrap TwoImg">
                            <img src="assets/img/tempimg/construction.png" alt="">
                            <img src="assets/img/tempimg/construction-2.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-4">
                        <h2 class="NormalHeading">Spaces</h2>
                        <p>"Space is the body language of an organization"<br> Max House will provide accessible and multifunctional spaces that are both indoors and outdoors, including various decompression zones for people to relax and meditate. The tenant floors have a clean rectangular floor plate, with the lift core being on one side of the building. Designed such that 90% of regularly occupied space gets direct line-of-sight to the outside environment, the design enables the ability to flexibly to have multiple tenants on a floor, without compromising the tenant experience.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/spaces.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-5">
                        <h2 class="NormalHeading">Sustainability LEED Gold Certified</h2>
                        <p>"Ultimately, we are responsible for building the future we want." <br><br>
                        <strong> Max House is designed to be LEED Gold certified. </strong> <br><br>
                        Max House is a thought leader in sustainability and aims to minimise its ecological footprint. To do so is important to us because we feel a certain responsibility towards our planet, and we invite you to share our enthusiasm for the same. The LEED Gold certification is a validation of our efforts and helps cement our belief that ecology, biophilia, commerce and real estate can co-exist at a single, iconic address.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/leed-gold.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-6">
                        <h2 class="NormalHeading">The philosophy of biophilia runs deep throughout the developments of Max Estates</h2>
                        <p>The natural world has inspired not just poets and philosophers through human history, but also scientists and entrepreneurs. It is only natural, then, that the place where you spend most of your day provides you with a connection to the natural world.</p>
                        <p> From the unique glass fitted into the façade to the very air you breathe inside Max House, everything you experience is designed to enhance the effects of nature on creativity, productivity and wellness. The three-tier air treatment technology delivers air quality on par with global standards; complete with CO2 sensors used to optimize ventilation and circulation.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/max-estate-22.png" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Key Fact Table -->
<section class="Section ThreeBoxTableSection lightPinkSection">
    <div class="container">
        <div class="MainHeading">
            <h2>Key Facts</h2>
        </div>
        <div class="ThreeBoxTableWrap">
            <!-- Box 1 -->
            <div class="BoxTableContainer">
                <ul>
                    <li>
                        <span>Type of Building</span>
                        <span>
                            <ol>
                                <li>IT / ITes</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Plot Size</span>
                        <span>
                            <ol>
                                <li>2868 sq. mtrs.</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Number of Floors</span>
                        <span>
                            <ol>
                                <li>Basement</li>
                                <li>Stilt</li>
                                <li>2 Podium</li>
                                <li>8 Tenant Floors</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Building Height</span>
                        <span>
                            <ol>
                                <li>40 m</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Typical Floor Size</span>
                        <span>
                            <ol>
                                <li>13000 sq. ft.</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Nearest Metro Station</span>
                        <span>
                            <ol>
                                <li>Okhla NSIC Metro Station (0.5 km)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Super area</span>
                        <span>
                            <ol>
                                <li>105,425 sq. ft. (approximation)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Floor Efficiency</span>
                        <span>
                            <ol>
                                <li>65% (+/- 2%)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Floor Condition</span>
                        <span>
                            <ol>
                                <li>Warmshell with screeding</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Washrooms (per floor)</span>
                        <span>
                            <ol>
                                <li>1 Gents, 1 Ladies & 1 Special Needs</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Car Parking Ratio</span>
                        <span>
                            <ol>
                                <li>1:1000</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Parking Levels</span>
                        <span>
                            <ol>
                                <li>Parking is in Basement + Stilt + Podium</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Number of Elevators</span>
                        <span>
                            <ol>
                                <li>3 passenger lifts</li>
                                <li>1 service lift</li>
                            </ol>
                        </span>
                    </li>
                </ul>
            </div>
            <!-- Box 2 -->
            <div class="BoxTableContainer LeftIconList CenterBoxTableContainer">
                <ul>
                    <li>
                        <span>Speed and capacity of elevators</span>
                        <span>
                            <ol>
                                <li>3 passenger lifts (13 passenger, 1.75 mps)</li>
                                <li>1 service lift (15 passenger, 1.75 mps)</li>
                                <li>2 car lifts (2500 kg, 0.4 mps)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Screeding specifications</span>
                        <span>
                            <ol>
                                <li>60mm screeding on all floors</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Chiller</span>
                        <span>
                            <ol>
                                <li>TRANE</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Capacity of Chiller</span>
                        <span>
                            <ol>
                                <li>265 TR</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Capacity of AHUs (Tr)</span>
                        <span>
                            <ol>
                                <li>2 AHU</li>
                                <li>7500 cfm,</li>
                                <li>Total heat gain : 344,327BTU/hr,</li>
                                <li>28.6 Tr. each</li>
                                <li>1 FCU - 1.5 Tr.</li>
                                <li>Merv 8 & Merv 13 Filters</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Air-conditioning Temperature inside</span>
                        <span>
                            <ol>
                                <li>23 ± 1 ºC</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Temp. of chilled water which shall be maintained during operation of HVAC system</span>
                        <span>
                            <ol>
                                <li>7ºC</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Relative Humidity</span>
                        <span>
                            <ol>
                                <li>50% - 55%</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Exhaust for toilets</span>
                        <span>
                            <ol>
                                <li>Yes</li>
                            </ol>
                        </span>
                    </li>
                </ul>
            </div>
            <!-- Box 3 -->
            <div class="BoxTableContainer RightBoxTableContainer">
                <ul>
                    <li>
                        <span>Capacity of Transformers</span>
                        <span>
                            <ol>
                                <li>1000 KVA</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>DG set configuration</span>
                        <span>
                            <ol>
                                <li>625KVA x 1 nos</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Power Back up (KVA)</span>
                        <span>
                            <ol>
                                <li>625 KVA (DG)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Power Back up (KVA)</span>
                        <span>
                            <ol>
                                <li>625 KVA (DG)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Number of fire hydrants per floor</span>
                        <span>
                            <ol>
                                <li>2</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Outdoor fire hydrants</span>
                        <span>
                            <ol>
                                <li>Yes</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Connectivity with fire tenders</span>
                        <span>
                            <ol>
                                <li>Yes</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Fire detection</span>
                        <span>
                            <ol>
                                <li>Yes (in common area and AFC)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Fire control room/BMS room</span>
                        <span>
                            <ol>
                                <li>Yes (in stilt floor)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>PA system</span>
                        <span>
                            <ol>
                                <li>Yes (in common area)</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Distance of closest fire station</span>
                        <span>
                            <ol>
                                <li>Okhla Phase-1, 5.2 KM</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Availability of fire resistant doors with rating in fire-prone areas</span>
                        <span>
                            <ol>
                                <li>Yes, for 2 hours</li>
                            </ol>
                        </span>
                    </li>
                    <li>
                        <span>Connectivity of Sewage with main disposal system</span>
                        <span>
                            <ol>
                                <li>Common STP (70 KLD)</li>
                            </ol>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="Section FourGridsWithBorder WhiteSection">
    <div class="container SmallContainer">
        <div class="MultipleLogoSection">
            <div class="MainHeading">
                <h2>Lorem ipsum dolor sit amet</h2>
            </div>
            <ul class="BrandsLogo">
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
            </ul>
        </div>
        <ul class="GridsWithBorder">
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante Loremem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
        </ul>
    </div>
</section>

<!-- Footer form -->
<section class="Section FooterForm">
    <div class="container">
        <div class="FormBlockWithHeading CenterAlignForm">
            <h2>Register Your Interest</h2>
            <p>To arrange a call-back and schedule a visit to our experience center, please submit<br> your contact details:</p>
            <form action="">

                <div class="row">                
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="First Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Last Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="email" placeholder="Email"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Phone"></div>
                    <div class="col-12 col-md-12"><textarea class="button draw" placeholder="Mesaage"></textarea></div>
                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
                </div>      
                
            </form>
        </div>
    </div>
</section>



<?php @include('template-parts/footer.php') ?>