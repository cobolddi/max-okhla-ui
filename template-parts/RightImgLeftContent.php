<section class="RightImgLeftContent">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="LeftContentBlock">
					<h2><img src="assets/img/maxgroup.svg"><span>Max Group</span></h2>
					<p>The Max Group is a leading Indian multi-business conglomerate with interests in the Life Insurance, Healthcare, Real Estate and Senior Living industries. In FY2019, the Group recorded consolidated revenue of Rs. 24,134 Cr. It currently has a total customer base of 9 million, around 340 offices spread across India, an agency strength of ~46,000 and employee strength of more than 27,000. The Max Group comprises three holding companies, namely Max Financial Services, Max India and Max Ventures & Industries.</p>
					<p> The Group’s investor base includes marquee global financial institutions such as New York Life, KKR, IFC Washington, Vanguard, Ward Ferry, Briarwood Capital, Ellipsis Partners, Nomura, Aberdeen, First State Investments, Indgrowth Capital Fund, India Insight Value Fund, First Voyager, Eastspring, Target Asset Management, Baron and Doric Capital.</p>
					<div class="FounderBlock">
							<div class="FounderIcon">
								<img src="assets/img/founder.png" alt="">
							</div>
							<div class="FounderText">
								<h4>Mr. Analjit Singh</h4>
								<span>Founder and Chairman, Max Group</span>
							</div>
						</div>
					<a href="https://www.maxgroup.in/?__cf_chl_jschl_tk__=23aa1448e1442c753b05ad54c6480c81dc4557f3-1587553881-0-AVpfJIK9bcswOTdKk6zDnzfRa_TOpo3xE41mZjedn5vdgIxnxe0JSbXaiNSengvakvfr3fxlFGitUmaGL1gQTeHosejCQutA6jrruMrOkwq-YM2rb6HkY-QzGCaJZ4uZwsyHehA1m-HNBgKCmg3SqWRWpk57SI_c3abjVtcS1fpid7t1N4sZhT-Z9Ah22M2s4pP7ORdhSFnUoYKMcckqlRwXNkAHU0rGr_e26gj1L915JBE1I8DGnF8rNECV0M7g-mHkluVirD1VIqaImk8M7ZU" target="_blank" class="BtnWithBorder MaroonBorder MTop0">Know More</a>
				</div>
			</div>
		</div>
		<div class="RightImgBox">
			<img src="assets/img/max-group.png" alt="" style="height: 620px;">
		</div>
	</div>
	
</section>