<section class="HomeBanner">
		<picture>
	    	<source srcset="assets/img/tempimg/banner.png" media="(min-width: 768px)">
		    <img src="assets/img/tempimg/banner.png" alt="" width="100%;" class="DesktopOnly">
		    <img src="assets/img/tempimg/mobile.png" alt="" width="100%;" class="MobileOnly">
		</picture>
		<div class="BannerContent">
			<div class="container">
				<div class="BannerContentBlock">
					<h5>Max House Okhla</h5>
					<h1>Open for Business <br>In the heart of New Delhi</h1>
					<a href="#" class="ButtonWithArw">Know More <img src="assets/img/arw-right.svg"> </a>
				</div>
			</div>
		</div>
	</div>	
	<!-- <div class="MobileOnly">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<picture>
				    	<source srcset="assets/img/tempimg/banner.png" media="(min-width: 768px)">
					    <img src="assets/img/tempimg/mobile.png" alt="" width="100%;">
					</picture>
				</div>
				<div class="col-12 col-md-6">
					<div class="BannerContentBlock">
						<h5>Max House Okhla</h5>
						<h1>Open for Business in the heart of New Delhi</h1>
						<h5><a href="tel:+91-9555395222">+91-9555395222</a><span>|</span><a href="mailto:leasing@maxestates.in">leasing@maxestates.in</a></h5>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	
</section>