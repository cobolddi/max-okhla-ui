<section class="Section ImageGallerySection">
	<div class="container">
		<div class="TopHeading">
			<h2>Gallery from projects</h2>
			<p>Explore photos and videos from projects , portfolios, testimonials and more.</p>
		</div>
		<div class="row">
            <div class="col-12 col-md-6 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                    </a>
                    <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-6 col-md-6">
                        <div class="row ImageGalleryInnerRow">
                            <div class="col-md-12 MBottom2">
                                <div class="ImageGalleryBox">
                                    <a href="assets/img/tempimg/gallery-3.png" class="ImageGalleryPopup">
                                        <div class="ImageGalleryOverlay">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                            </svg>
                                        </div>
                                        <img src="assets/img/tempimg/gallery-3.png" alt="Image">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-12 MBottom2">
                                <div class="ImageGalleryBox">
                                    <a href="assets/img/tempimg/gallery-3.png" class="ImageGalleryPopup">
                                        <div class="ImageGalleryOverlay">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                            </svg>
                                        </div>
                                        <img src="assets/img/tempimg/gallery-3.png" alt="Image">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-6">
                        <div class="ImageGalleryBox">
                            <a href="assets/img/tempimg/gallery-4.png" class="ImageGalleryPopup">
                                <div class="ImageGalleryOverlay">
                                    <svg class="Icon">
                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                    </svg>
                                </div>
                                <img src="assets/img/tempimg/gallery-4.png" alt="Image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4 col-md-4 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-5.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                        <img src="assets/img/tempimg/gallery-5.png" alt="Image">
                    </a>
                </div>
            </div>
            <div class="col-4 col-md-4 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-6.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                        <img src="assets/img/tempimg/gallery-6.png" alt="Image">
                    </a>
                </div>
            </div>
            <div class="col-4 col-md-4 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-7.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                        <img src="assets/img/tempimg/gallery-7.png" alt="Image">
                    </a>
                </div>
            </div>
        </div>
        <div class="row LoadMoreImageRow">
            <div class="col-4 col-md-4 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                    </a>
                    <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                </div>
            </div>
            <div class="col-4 col-md-4 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                    </a>
                    <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                </div>
            </div>
            <div class="col-4 col-md-4 MBottom2">
                <div class="ImageGalleryBox">
                    <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                        <div class="ImageGalleryOverlay">
                            <svg class="Icon">
                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                            </svg>
                        </div>
                    </a>
                    <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                </div>
            </div>
        </div>
		<div class="BtnWrap CenterAlign">
			<a href="#" class="Button BtnWithBorder MaroonBorder">Load more</a>
		</div>
	</div>
</section>