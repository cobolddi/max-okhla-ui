<section class="Section">
	<div class="NoLeftRightSpace LeftImgRightContentWithBg">
		<div class="LeftImage">
			<img src="assets/img/max.png" alt="">
		</div>
		<div class="RightContent">
			<h2>Max Estates</h2>
			<p>Established in 2016, Max Estates Limited is the real estate arm of Max Group with the vision to bring the Group’s values of Sevabhav, Excellence and Credibility to the Indian real estate sector.</p> 
			<p>The mission of Max Estates is to offer spaces for commercial use with utmost attention to detail, design and lifestyle. With a team consisting of engineers, architects, planners and specialists, and collaborations with global leaders in design, master planning, landscape and sustainability, Max Estates is committed to delivering a truly unique quality of excellence and lifestyle to all our customers. Max Estates is a subsidiary of Max Ventures and Industries Limited (MVIL).</p>
			<a href="#" class="BtnWithBorder">Know More</a>
		</div>
	</div>
</section>