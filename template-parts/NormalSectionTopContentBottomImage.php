<section class="Section">
	<div class="container">
		<div class="NormalSection">
			<div class="NormalSectionContainer">
				<h2 class="NormalHeading">Design that is Iconic & Sublime</h2>
				<p>Designed meticulously to be highly functional while being aesthetically pleasing. The unique façade, made of terracotta bricks, showcases the rich industrial history of Okhla. The iconic clock tower on the South-West side of the building makes Max House an unmissable address.</p> 
				<p>As a result, the workspace we offer our tenants acts as an area of advantage for retention, recruitment, development, and well-being. Hence, we wanted to re-think the workspace to not only be a real estate asset but a strategic asset for our tenants.</p>
				<div class="imgWrap TwoImg">
					<img src="assets/img/tempimg/iconic-sublime-1.png" alt="">
					<img src="assets/img/tempimg/iconic-sublime-2.png" alt="">
				</div>
			</div>
			<div class="NormalSectionContainer">
				<h2 class="NormalHeading">Workwell</h2>
				<p>Designed meticulously to be highly functional while being aesthetically pleasing. The unique façade, made of terracotta bricks, showcases the rich industrial history of Okhla. The iconic clock tower on the South-West side of the building makes Max House an unmissable address.</p> 
				<p>As a result, the workspace we offer our tenants acts as an area of advantage for retention, recruitment, development, and well-being. Hence, we wanted to re-think the workspace to not only be a real estate asset but a strategic asset for our tenants.</p>
				<div class="imgWrap">
					<img src="assets/img/tempimg/workwell.png" alt="">
				</div>
			</div>
			<div class="NormalSectionContainer">
				<h2 class="NormalHeading">Simply Superior Materials & Construction</h2>
				<p>The materials used to build each space have been carefully chosen to maintain a sense of luxury and balancing it with our high sustainability design standards. From unique glass bricks to terracotta brick tiles, everything at Max House has been hand-picked to ensure a sense of luxury, comfort and longevity.</p> 
				<p>The common areas of Max House are well appointed with: <br> A combination of brick and marble which exude a sense of welcome and warmth Double glazed windows to lower operating costs while allowing light transmission Wooden wall finish & panelling High ceilings with a height of 3.75 meters Efficient floor plates with a clean, efficient rectangular design Plants and nature wherever you look</p>
				<div class="imgWrap TwoImg">
					<img src="assets/img/tempimg/construction.png" alt="">
					<img src="assets/img/tempimg/construction-2.png" alt="">
				</div>
			</div>
			<div class="NormalSectionContainer">
				<h2 class="NormalHeading">Spaces</h2>
				<p>"Space is the body language of an organization"<br> Max House will provide accessible and multifunctional spaces that are both indoors and outdoors, including various decompression zones for people to relax and meditate. The tenant floors have a clean rectangular floor plate, with the lift core being on one side of the building. Designed such that 90% of regularly occupied space gets direct line-of-sight to the outside environment, the design enables the ability to flexibly to have multiple tenants on a floor, without compromising the tenant experience.</p>
				<div class="imgWrap">
					<img src="assets/img/tempimg/spaces.png" alt="">
				</div>
			</div>
			<div class="NormalSectionContainer">
				<h2 class="NormalHeading">Sustainability LEED Gold Certified</h2>
				<p>"Ultimately, we are responsible for building the future we want." <br><br>
				<strong> Max House is designed to be LEED Gold certified. </strong> <br><br>
				Max House is a thought leader in sustainability and aims to minimise its ecological footprint. To do so is important to us because we feel a certain responsibility towards our planet, and we invite you to share our enthusiasm for the same. The LEED Gold certification is a validation of our efforts and helps cement our belief that ecology, biophilia, commerce and real estate can co-exist at a single, iconic address.</p>
				<div class="imgWrap">
					<img src="assets/img/tempimg/leed-gold.png" alt="">
				</div>
			</div>
			<div class="NormalSectionContainer">
				<h2 class="NormalHeading">The philosophy of biophilia runs deep throughout the developments of Max Estates</h2>
				<p>The natural world has inspired not just poets and philosophers through human history, but also scientists and entrepreneurs. It is only natural, then, that the place where you spend most of your day provides you with a connection to the natural world.</p>
				<p> From the unique glass fitted into the façade to the very air you breathe inside Max House, everything you experience is designed to enhance the effects of nature on creativity, productivity and wellness. The three-tier air treatment technology delivers air quality on par with global standards; complete with CO2 sensors used to optimize ventilation and circulation.</p>
				<div class="imgWrap">
					<img src="assets/img/tempimg/max-estate-22.png" alt="image">
				</div>
			</div>
		</div>
	</div>
</section>