<section class="Section">
	<div class="assideContainer">
		<ul class="assideInner">
			<li>
				<a href="" class="is_active">Design that is Iconic & Sublime</a>
			</li>
			<li>
				<a href="">Workwell</a>
			</li>
			<li>
				<a href="">Superior Construction Material</a>
			</li>
			<li>
				<a href="">Multifunctional Spaces</a>
			</li>
			<li>
				<a href="">Sustainability LEED Gold Certified</a>
			</li>
			<li>
				<a href="">The philosophy of biophilia</a>
			</li>
		</ul>
	</div>
</section>