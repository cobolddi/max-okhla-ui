<section class="Section ThreeBoxTableSection lightPinkSection">
	<div class="container">
		<div class="MainHeading">
			<h2>Key Facts</h2>
		</div>
		<div class="ThreeBoxTableWrap">
			<!-- Box 1 -->
			<div class="BoxTableContainer">
				<!-- <div class="TableHead">
					<div class="HeadingWrap">
						<h2>Traditional Offices</h2>
					</div>
				</div> -->
				<ul>
					<li>
						<span>Type of Building</span>
						<span>
							<ol>
								<li>IT / ITes</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Plot Size</span>
						<span>
							<ol>
								<li>2868 sq. mtrs.</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Number of Floors</span>
						<span>
							<ol>
								<li>Basement</li>
								<li>Stilt</li>
								<li>2 Podium</li>
								<li>8 Tenant Floors</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Building Height</span>
						<span>
							<ol>
								<li>40 m</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Typical Floor Size</span>
						<span>
							<ol>
								<li>13000 sq. ft.</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Nearest Metro Station</span>
						<span>
							<ol>
								<li>Okhla NSIC Metro Station (0.5 km)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Super area</span>
						<span>
							<ol>
								<li>105,425 sq. ft. (approximation)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Floor Efficiency</span>
						<span>
							<ol>
								<li>65% (+/- 2%)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Floor Condition</span>
						<span>
							<ol>
								<li>Warmshell with screeding</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Washrooms (per floor)</span>
						<span>
							<ol>
								<li>1 Gents, 1 Ladies & 1 Special Needs</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Car Parking Ratio</span>
						<span>
							<ol>
								<li>1:1000</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Parking Levels</span>
						<span>
							<ol>
								<li>Parking is in Basement + Stilt + Podium</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Number of Elevators</span>
						<span>
							<ol>
								<li>3 passenger lifts</li>
								<li>1 service lift</li>
							</ol>
						</span>
					</li>
				</ul>
			</div>
			<!-- Box 2 -->
			<div class="BoxTableContainer LeftIconList CenterBoxTableContainer">
				<!-- <div class="TableHead">
					<div class="HeadingWrap">
						<h2>VS</h2>
					</div>
				</div> -->
				<ul>
					<li>
						<span>Speed and capacity of elevators</span>
						<span>
							<ol>
								<li>3 passenger lifts (13 passenger, 1.75 mps)</li>
								<li>1 service lift (15 passenger, 1.75 mps)</li>
								<li>2 car lifts (2500 kg, 0.4 mps)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Screeding specifications</span>
						<span>
							<ol>
								<li>60mm screeding on all floors</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Chiller</span>
						<span>
							<ol>
								<li>TRANE</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Capacity of Chiller</span>
						<span>
							<ol>
								<li>265 TR</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Capacity of AHUs (Tr)</span>
						<span>
							<ol>
								<li>2 AHU</li>
								<li>7500 cfm,</li>
								<li>Total heat gain : 344,327BTU/hr,</li>
								<li>28.6 Tr. each</li>
								<li>1 FCU - 1.5 Tr.</li>
								<li>Merv 8 & Merv 13 Filters</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Air-conditioning Temperature inside</span>
						<span>
							<ol>
								<li>23 ± 1 ºC</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Temp. of chilled water which shall be maintained during operation of HVAC system</span>
						<span>
							<ol>
								<li>7ºC</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Relative Humidity</span>
						<span>
							<ol>
								<li>50% - 55%</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Exhaust for toilets</span>
						<span>
							<ol>
								<li>Yes</li>
							</ol>
						</span>
					</li>
				</ul>
			</div>
			<!-- Box 3 -->
			<div class="BoxTableContainer RightBoxTableContainer">
				<!-- <div class="TableHead">
					<div class="HeadingWrap">
						<h2>Managed Offices by MAX</h2>
					</div>
				</div> -->
				<ul>
					<li>
						<span>Capacity of Transformers</span>
						<span>
							<ol>
								<li>1000 KVA</li>
							</ol>
						</span>
					</li>
					<li>
						<span>DG set configuration</span>
						<span>
							<ol>
								<li>625KVA x 1 nos</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Power Back up (KVA)</span>
						<span>
							<ol>
								<li>625 KVA (DG)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Power Back up (KVA)</span>
						<span>
							<ol>
								<li>625 KVA (DG)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Number of fire hydrants per floor</span>
						<span>
							<ol>
								<li>2</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Outdoor fire hydrants</span>
						<span>
							<ol>
								<li>Yes</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Connectivity with fire tenders</span>
						<span>
							<ol>
								<li>Yes</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Fire detection</span>
						<span>
							<ol>
								<li>Yes (in common area and AFC)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Fire control room/BMS room</span>
						<span>
							<ol>
								<li>Yes (in stilt floor)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>PA system</span>
						<span>
							<ol>
								<li>Yes (in common area)</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Distance of closest fire station</span>
						<span>
							<ol>
								<li>Okhla Phase-1, 5.2 KM</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Availability of fire resistant doors with rating in fire-prone areas</span>
						<span>
							<ol>
								<li>Yes, for 2 hours</li>
							</ol>
						</span>
					</li>
					<li>
						<span>Connectivity of Sewage with main disposal system</span>
						<span>
							<ol>
								<li>Common STP (70 KLD)</li>
							</ol>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>