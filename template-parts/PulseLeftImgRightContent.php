<section class="PulseLeftImgRightContent SectionBottom5">
	<div class="LeftImgBox">
		<span class="BgImg"><!-- <img src="assets/img/building.png" alt=""> --></span>
		<span class="overlapImg"><img src="assets/img/maxgroup.png" alt=""></span>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 col-lg-6">
				
			</div>
			<div class="col-12 col-md-6 col-lg-6">
				<div class="PulseRightContent">
					<div class="MaxPhilosophy">
						<h2>Max House Philosophy</h2>
						<p>Max House is an attempt to provide you with an iconic design and superior hospitality to create an environment, which integrates work and life, nurturing a more productive, healthier and happier community.</p>
						<ul>
							<li><span>Unique Design:</span> Facade made of terracotta bricks showcasing the rich industrial history of Okhla</li>
							<li><span>Campus environment:</span> Max House is located with an approx. 2-acre campus, thus providing ease of parking, walking spaces, landscape, security, movement and access</li>
							<li><span>High Ceilings:</span> Floor to floor height of 3.75 meters. to achieve greater finished height</li>
							<li><span>Sustainability:</span> Designed with the intent of achieving a LEED Gold rating, the materials used in the building have been carefully chosen to maintain the high sustainability standards</li>
							<li><span>Efficient floor plates:</span> With the lift core on one side, tenants get a clean, efficient rectangular floor plate of approx. 12,000 sq. ft.</li>
							<li><span>Indoor Air Quality:</span> 3 tier air treatment technology will deliver air quality on par with global standards; complete with CO2 sensors used to optimize ventilation and circulation</li>
							<li><span>Wellness:</span> Large landscaped spaces</li>
							<li><span>Food & Beverage:</span> Terrace restaurant and a vibrant café on the ground floor</li>
						</ul>
					</div>
					<div class="PulseIconBox">
						<span><img src="assets/img/PulseLogo.svg" alt=""></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>