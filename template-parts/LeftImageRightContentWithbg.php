<section class="Section">
	<div class="LeftImgRightContentWithBg">
		<div class="LeftImage">
			<img src="assets/img/about.png" alt="">
		</div>
		<div class="RightContent">
			<h2>Max House Okhla</h2>
			<p>Originally the headquarters of the $3B Max Group, Max House is located at the epicenter of the Secondary Business District of Delhi. Offering ~105,000 sq. ft. of prime real estate spread across 10 floors, Max House is poised to be the new business address in Delhi NCR.</p>
			<p>Designed to address the future of work while considering human capital to be an occupier’s most important resource, Max House blends thoughtful design and superior hospitality in order to nurture a more productive, healthier and happier community.</p>
		</div>
	</div>
</section>