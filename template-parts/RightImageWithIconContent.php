<section class="RightImgLeftContent RightImgWithIconContent">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-7">
				<div class="LeftContentBlock">
					<h2>Key Highlights of Okhla</h2>
					<p>Designed meticulously to be highly functional while being aesthetically pleasing.<br>The unique façade, made of terracotta bricks, showcases the rich industrial <br>history of Okhla.</p>
					<div class="LeftIconBlock">
						<div class="row">
							<div class="col-6 col-md-4">
								<div class="TopIconBottomText">
									<span><img src="assets/img/location.svg" alt=""></span>
									<h5>Locational advantage</h5>
									<p>Well connected to all regions of NCR</p>
								</div>
							</div>
							<div class="col-6 col-md-4">
								<div class="TopIconBottomText">
									<span><img src="assets/img/accessibility.svg" alt=""></span>
									<h5>Accessibility</h5>
									<p>Excellent connectivity with the nearby Metro station and railway station</p>
								</div>
							</div>
							<div class="col-6 col-md-4">
								<div class="TopIconBottomText">
									<span><img src="assets/img/establish.svg" alt=""></span>
									<h5>Established marke</h5>
									<p>One of the most prominent business-centric location in NCR</p>
								</div>
							</div>
							<div class="col-6 col-md-4">
								<div class="TopIconBottomText">
									<span><img src="assets/img/infra.svg" alt=""></span>
									<h5>Ready Social Infrastructure</h5>
									<p>Lotus Temple, F&B outlets, hospitals, hotels, etc</p>
								</div>
							</div>
							<div class="col-6 col-md-4">
								<div class="TopIconBottomText">
									<span><img src="assets/img/rich.svg" alt=""></span>
									<h5>Rich Cultural Heritage</h5>
									<p>Surrounded by heritage sites and boundless art history of Delhi.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-5">
				<div class="RightImgBox">
					<img src="assets/img/Elevator-Lobby.png" alt="" style="">
				</div>
			</div>
		</div>
	</div>
	<div class="DesktopOnly">
		<div class="RightImgBox">
			<img src="assets/img/Elevator-Lobby.png" alt="" style="">
		</div>
	</div>
	
    <div class="FloatingImage">
        <img src="assets/img/Lobby1.png" alt="">
    </div>
</section>