
    </main>

<!-- <section class="Section FooterForm">
    <div class="container">
        <div class="FormBlockWithHeading">
            <h2>Register Your Interest</h2>
            <p>To arrange a call-back and schedule a visit to our experience center, please submit<br> your contact details:</p>
            <form action="">

                <div class="row">                
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="First Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Last Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="email" placeholder="Email"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Phone"></div>
                    <div class="col-12 col-md-12"><textarea class="button draw" placeholder="Mesaage"></textarea></div>
                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
                </div>      
                
            </form>
        </div>
    </div>
</section> -->

<footer>
    <div class="Section TopFooter">
        <div class="container">
            <div class="FooterDetails">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h5>Connect With Us</h5>
                        <p>Alternatively, you may contact us at <br><a href="tel:+91-9555395222">+91-9555395222</a> <br><a href="mailto:info@maxhouseokhla.in">info@maxhouseokhla.in</a></p>
                    </div>
                    <div class="col-6 col-md-2">
                        <h5>Quick Links</h5>
                        <ul class="footerNav">
                            <li><a href="#">Max Estates</a></li>
                            <li><a href="#">222 Rajpur</a></li>
                            <li><a href="#">MVIL</a></li>
                            <li><a href="#">MSFL</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-2">
                        <h5 class="FollowHeads">Follow Us On</h5>
                        <ul class="SocialButton">
                            <li>
                                <a href="#"><img src="assets/img/twitter.svg" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/linkedin.svg" alt=""></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-4">
                        <a href="index.php" class="Footerlogo"><img src="assets/img/footerlogo.svg"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="MaxImage">
            <img src="assets/img/footerimg.png" alt="">
        </div>
    </div>
    <div class="DesclaimerBox">
        <div class="container">
            <h6>Disclamier</h6>
            <p>This presentation is an artistic representation of the building/project and is purely conceptual. Images used for spaces at Max House Okhla are artists’ impressions for representational purposes only. This is not a legal offering/offer to sell. Further, promoters/architects reserve the right to add or delete any detail, features, specifications, elevations, etc. mentioned in this brochure if so warranted by the circumstances/directions by the competent authority. Some of the above-mentioned features/ amenities are currently in the process of being incorporated into the building.</p>
            <a href="" class="privacyPolicyLink">Privacy Policy</a>
        </div>
    </div>
    <div class="Copyrightbox">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 DesktopOnly">
                    
                </div>
                <div class="col-12 col-md-7">
                    <div class="CopyrightText">
                        <span>Copyright © 2020, Max House Okhla.</span>
                        <span><a href="cobold.in" target="_blank"><img src="assets/img/cobolddigital.svg"></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#" id="scroll" style="display: none;"><img src="assets/img/scrolltotop.svg" alt=""></a>

<!-- page -->
        

<script src="assets/js/vendor.js"></script>
<script src="assets/js/scripts.js"></script>

</body>
</html>