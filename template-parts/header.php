<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Max House Okhla</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">
	
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
</head>
<body>
	<header class="Header" id="header">
		<div class="container">
			<div class="MenuContainer">
				<div class="LogoContainer">
					<a href="index.php"><img src="assets/img/logo.png"></a>
				</div>
				<div class="NavContainer">
					<nav>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><a href="design.php">Design</a></li>
							<li><a href="location.php">Location</a></li>
							<li><a href="pulse.php">Pulse</a></li>
							<li><a href="news-updates.php">News & Updates</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</nav>
					<div class="MenuOverlay MobileNavController">
						<a class="CloseMenuIcon MobileNavController" href="#">
							<span>&times;</span>
						</a>
					</div>
					<a class="MenuBar MobileNavController" href="#">
						<div>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</header>
	<main>
	

	