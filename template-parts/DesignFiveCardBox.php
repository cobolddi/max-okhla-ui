<section class="Section DesignFiveCardBlock">
	<div class="Container">
		<div class="HeadingContent">
			<h2>Design Philosophy</h2>
			<p>Designed meticulously to be highly functional while being aesthetically pleasing. The unique façade, made of terracotta bricks, showcases the rich industrial history of Okhla. The iconic clock tower on the South-West side of the building makes Max House an unmissable address.</p>
		</div>
		<ul>
			<li>
				<img src="assets/img/Security.png" alt="">
				<h5>Iconic Design</h5>
			</li>
			<li>
				<img src="assets/img/superior.png" alt="">
				<h5>Superior Construction Material</h5>
			</li>
			<li>
				<img src="assets/img/space.png" alt="">
				<h5>Multifunctional Spaces</h5>
			</li>
			<li>
				<img src="assets/img/leed.png" alt="">
				<h5>LEED Gold Certified</h5>
			</li>
			<li>
				<img src="assets/img/biophile.png" alt="">
				<h5>The philosophy of biophilia</h5>
			</li>
			<!-- <li class="MobileOnly">
				<div class="BtnBlock">
					<a href="#" class="BtnWithBorder MTop0">Know More</a>			
				</div>
			</li> -->
		</ul>
		<!-- <div class="DesktopOnly"> -->
			<div class="BtnBlock">
				<a href="#" class="BtnWithBorder MTop0">Know More</a>			
			</div>
		<!-- </div> -->
	</div>
</section>