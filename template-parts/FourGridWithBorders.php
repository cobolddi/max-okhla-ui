<section class="Section FourGridsWithBorder WhiteSection">
	<div class="container SmallContainer">
		<div class="MultipleLogoSection">
			<div class="MainHeading">
				<h2>Lorem ipsum dolor sit amet</h2>
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit magnam eaque nihil. Minima minus vero in dicta pariatur deserunt, commodi.</p> -->
			</div>
			<ul class="BrandsLogo">
				<li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
				<li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
				<li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
			</ul>
		</div>
		<ul class="GridsWithBorder">
			<li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
		</ul>
	</div>
</section>