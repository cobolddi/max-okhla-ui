jQuery(function($){
  'use-strict'
  $(document).ready(function() {
    
    //-----HeaderPadding
    var headerHeight = $('#header').outerHeight(true);
    // console.log("Header Height", headerHeight);
    $('main').css('paddingTop', headerHeight);

    // Onscroll header Background
    $(window).scroll(function(){
      if ($(this).scrollTop() > 20) {
         $('#header').addClass('newClass');
      } else {
         $('#header').removeClass('newClass');
      }
  });

    //Mobile menu
    $(".MenuBar").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").addClass("Active_Nav");
    });
    $(".MenuOverlay").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").removeClass("Active_Nav");
    });

    function checkScrennSize(){
      if (window.matchMedia('(max-width: 991px)').matches) {
          $(".haveSubmenu>a").append('<span><img src="assets/img/chevron-down-white.svg"></span>');
      }
    }
    checkScrennSize();

    $(".haveSubmenu").find('a>span').click(function(e){
    // $(".haveSubmenu>a>span").click(function(e){
      // e.stopPropagation();
      e.preventDefault();
      $(this).parents('a').parent().toggleClass("Active_MobileDropdown");
    });

    // Scroll to top

    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 50) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 800); 
        return false; 
    }); 

    // Inside Banner Slider 
    $(".InsideBanner").slick({
        rtl:false, // If RTL Make it true & .slick-slide{float:right;}
        autoplay:true, 
        autoplaySpeed:5000, //  Slide Delay
        speed:800, // Transition Speed
        slidesToShow:1, // Number Of Carousel
        slidesToScroll:1, // Slide To Move 
        pauseOnHover:false,
        // appendArrows:$(".SliderContainer .Head .Arrows"), // Class For Arrows Buttons
        prevArrow:'<span class="Slick-Prev"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
        nextArrow:'<span class="Slick-Next"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
        easing:"linear",
      })

    // Gallery Popup
    //Magnific popup Gallery
    $(".ImageGalleryPopup").magnificPopup({
      type: 'image',
      mainClass: 'mfp-with-zoom',
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: false,
        duration: 300,
        easing: 'ease-in-out',
        opener: function(openerElement) {
          // return openerElement.is('img') ? openerElement : openerElement.find('img');
          return openerElement ;
        }
      }
    });

    //Gallery Load More
    function showMoreImage(showImage, selector){
      // console.log('showImage', showImage);
      var Increment = 0;
      $(selector).each(function(){
        // var This = this;
        var LoadMore = $(this).find(".Button");
        var ImageDiv = $(this).find('.LoadMoreImageRow>div');
        var ImageDiv_length = ImageDiv.length;
        // console.log("ImageDiv Length", ImageDiv_length);
        var ImageArray = Array.prototype.slice.call(ImageDiv);
        var ImageArray_Length = ImageArray.length;
        // console.log("ImageArray_Length", ImageArray_Length);

        if(ImageArray <= 0){
          LoadMore.hide();
        }

        ImageDiv.hide();
        
        var slicedDiv = ImageArray.slice(0, 0);
        // console.log('slicedDiv', slicedDiv);
        $(slicedDiv).fadeIn('slow');
        
        $(LoadMore).on('click', function(e){
          e.preventDefault();
          Increment += showImage;
          slicedDiv = ImageArray.slice(0, Increment);
          $(slicedDiv).fadeIn('slow');
          var slicedDiv_length = $(slicedDiv).length;
          // console.log('slicedDiv_length', slicedDiv_length);
          if( slicedDiv_length >= ImageArray_Length ){
            LoadMore.fadeOut('slow');
          }
        });
        
      });
    }
    showMoreImage(3, '.ImageGallerySection');



    //Pulse Card
    var elementActiveNow = null;
    $('.SinglePulseCard a').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      elementActiveNow = $(this).closest('.row');
      elementActiveNow.hide('slow');
      var CardHref = $(this).attr('href');
      console.log('CardHref', CardHref);
      $(CardHref).slideDown(300, 'linear');
    });

    $('.ClosePulseCardContent').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      var CardId = $(this).parent().attr('id');
      elementActiveNow.show('slow');
      $('#' + CardId).slideUp('slow');
    })

    //Smooth scroll Sidebar
    $(".asideInner a[href^='#']").click(function(e) {
      e.preventDefault();
      $('.asideInner a.is_active').removeClass('is_active');
      $(this).addClass('is_active');
      
      var position = $($(this).attr("href")).offset().top;
      console.log('position', position);
      $("body, html").animate({
        scrollTop: position - headerHeight
      } /* speed */ );
    });

    // Sidebar sticky
    var NormalSection = $('.NormalSection').outerHeight();
    console.log('NormalSection', NormalSection);
    if($(window).width() >= 768 ){
      $(window).scroll(function(){
        var window_top = $(window).scrollTop() - 0;
        // console.log('window_top', window_top);
        if(window_top < NormalSection+200){
          $(".asideContainer").sticky({
            topSpacing: headerHeight
          });
        } else {
          $(".asideContainer").unstick()
        }
      });
    }

  }); //--\End document.ready();
});