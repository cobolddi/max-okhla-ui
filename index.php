<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageHeader/HomeBanner.php') ?>
<?php @include('template-parts/LeftImageRightContentWithbg.php') ?>
<?php @include('template-parts/RightImgLeftContent.php') ?>
<?php @include('template-parts/LeftImageRightContentWithNospace.php') ?>
<?php @include('template-parts/PulseLeftImgRightContent.php') ?>
<?php @include('template-parts/DesignFiveCardBox.php') ?>
<?php @include('template-parts/RightImageWithIconContent.php') ?>


<section class="Section FooterForm">
    <div class="container">
        <div class="FormBlockWithHeading ">
            <h2>Register Your Interest</h2>
            <p>To arrange a call-back and schedule a visit to our experience center, please submit<br> your contact details:</p>
            <form action="">

                <div class="row">                
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="First Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Last Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="email" placeholder="Email"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Phone"></div>
                    <div class="col-12 col-md-12"><textarea class="button draw" placeholder="Mesaage"></textarea></div>
                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
                </div>      
                
            </form>
        </div>
    </div>
</section>
<?php @include('template-parts/footer.php') ?>
