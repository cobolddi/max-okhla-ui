<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageHeader/InsideSliderBanner.php') ?>

<section class="Section DesignPage">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="assideContainer">
                    <ul class="assideInner">
                        <li>
                            <a href="#Container-1" class="is_active">Design that is Iconic & Sublime</a>
                        </li>
                        <li>
                            <a href="#Container-2">Workwell</a>
                        </li>
                        <li>
                            <a href="#Container-3">Superior Construction Material</a>
                        </li>
                        <li>
                            <a href="#Container-4">Multifunctional Spaces</a>
                        </li>
                        <li>
                            <a href="#Container-5">Sustainability LEED Gold Certified</a>
                        </li>
                        <li>
                            <a href="#Container-6">The philosophy of biophilia</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="NormalSection PTop12px">
                    <div class="NormalSectionContainer" id="Container-1">
                        <h2 class="NormalHeading">About Pulse</h2>
                        <!-- Pulse container -->
                        <div class="PulseCardContainer">
                            <div class="row" style="">
                                <div class="col-12 col-lg-6 SinglePulseCard PulseGreen">
                                    <a href="#PulseCommmunity" data-id="PulseCommmunity">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <!-- <img src="https://www.maxtowers.com/wp-content/uploads/2019/03/Community.png" alt=""> -->
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-community"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Community</div>
                                        </div>
                                    </a>
                                </div>
                            
                                <div class="col-12 col-lg-6 SinglePulseCard PulsePurple">
                                    <a href="#PulseConveniences" data-id="PulseConveniences">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plantHand"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Conveniences</div>
                                        </div>
                                    </a>
                                </div>
                            
                                <div class="col-12 col-lg-6 SinglePulseCard PulseYellow">
                                    <a href="#PulseTechnology" data-id="PulseTechnology">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-technology"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Technology</div>
                                        </div>
                                    </a>
                                </div>
                            
                                <div class="col-12 col-lg-6 SinglePulseCard PulsePink">
                                    <a href="#PulseDesignBuild" data-id="PulseDesignBuild">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-DesignBuild"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Design &amp; Build</div>
                                        </div>
                                    </a>
                                </div>
                                    
                            </div>

                            <div class="PulseCardContent PulseGreen" id="PulseCommmunity" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent PulseGreen">
                                    <div class="Image">
                                        <!-- <img src="https://www.maxtowers.com/wp-content/uploads/2019/03/Community.png" alt=""> -->
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-community"></use>
                                            </svg>      
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Community</span> - Enriching your lives through
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>Curation and execution of a monthly calendar of activities at Max Towers.</li>
                                        <li>Events on weekdays and weekends across a variety of genres that allow you the flexibility to pick and choose basis your convenience and interest.</li>
                                        <li>Easy access to casual and fine dining outlets for a work or personal get together or celebration.</li>
                                        <li>Preferred use of spaces and access to activities and initiatives on site.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="PulseCardContent PulsePurple" id="PulseConveniences" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent">
                                    <div class="Image">
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-plantHand"></use>
                                            </svg>       
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Conveniences</span> - Simplifying your lives by
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>Drawing the balance between work and life more favorably by moving your weekend chores on-site e.g. car service, salon visits, etc.</li>
                                        <li>Enabling you to get more done in life through easy access to weekday routine activities like workouts, swimming, groceries, laundry, etc.</li>
                                        <li>Providing a helping and caring hand to you through health checkups, pharmacy access, etc.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="PulseCardContent PulseYellow" id="PulseTechnology" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent">
                                    <div class="Image">
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-technology"></use>
                                            </svg>     
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Technology</span> - Enabling&nbsp;you to WorkWell&nbsp;through
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>A building app that connects you to others systems in the building – smart access, event ticketing, online food ordering, visitor management, meeting room booking, etc.</li>
                                        <li>An integrated visitor management system that allows you to seamlessly manage visitor movement.</li>
                                        <li>Meeting room booking system that enables you to efficiently manage meeting spaces.</li>
                                        <li>An intuitive building management system that drives efficiency and optimization in utilities.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="PulseCardContent PulsePink" id="PulseDesignBuild" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent">
                                    <div class="Image">
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-DesignBuild"></use>
                                            </svg>     
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Design &amp; Build</span> - Bringing&nbsp;in depth awareness around impact of design and build in your lives through
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>Increased biophilia&nbsp;throughout the&nbsp;building&nbsp;including your floor.</li>
                                        <li>Refresh your workspace aspects to create or reinforce your organization’s cultural ethos.</li>
                                        <li>Contemporary design incorporating modern workplace trends suited to your needs through our preferred award-winning design partners.</li>
                                        <li>Range of build-out services suiting your budget through our preferred fit-out partners.</li>
                                        <li>Incorporation of subtle elements that induce employee wellness.</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- End pulse container -->
                    </div>
                    <div class="NormalSectionContainer" id="Container-2">
                        <h2 class="NormalHeading">Workwell</h2>
                        <p>Designed meticulously to be highly functional while being aesthetically pleasing. The unique façade, made of terracotta bricks, showcases the rich industrial history of Okhla. The iconic clock tower on the South-West side of the building makes Max House an unmissable address.</p> 
                        <p>As a result, the workspace we offer our tenants acts as an area of advantage for retention, recruitment, development, and well-being. Hence, we wanted to re-think the workspace to not only be a real estate asset but a strategic asset for our tenants.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/workwell.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-3">
                        <h2 class="NormalHeading">Simply Superior Materials & Construction</h2>
                        <p>The materials used to build each space have been carefully chosen to maintain a sense of luxury and balancing it with our high sustainability design standards. From unique glass bricks to terracotta brick tiles, everything at Max House has been hand-picked to ensure a sense of luxury, comfort and longevity.</p> 
                        <p>The common areas of Max House are well appointed with: <br> A combination of brick and marble which exude a sense of welcome and warmth Double glazed windows to lower operating costs while allowing light transmission Wooden wall finish & panelling High ceilings with a height of 3.75 meters Efficient floor plates with a clean, efficient rectangular design Plants and nature wherever you look</p>
                        <div class="imgWrap TwoImg">
                            <img src="assets/img/tempimg/construction.png" alt="">
                            <img src="assets/img/tempimg/construction-2.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-4">
                        <h2 class="NormalHeading">Spaces</h2>
                        <p>"Space is the body language of an organization"<br> Max House will provide accessible and multifunctional spaces that are both indoors and outdoors, including various decompression zones for people to relax and meditate. The tenant floors have a clean rectangular floor plate, with the lift core being on one side of the building. Designed such that 90% of regularly occupied space gets direct line-of-sight to the outside environment, the design enables the ability to flexibly to have multiple tenants on a floor, without compromising the tenant experience.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/spaces.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-5">
                        <h2 class="NormalHeading">Sustainability LEED Gold Certified</h2>
                        <p>"Ultimately, we are responsible for building the future we want." <br><br>
                        <strong> Max House is designed to be LEED Gold certified. </strong> <br><br>
                        Max House is a thought leader in sustainability and aims to minimise its ecological footprint. To do so is important to us because we feel a certain responsibility towards our planet, and we invite you to share our enthusiasm for the same. The LEED Gold certification is a validation of our efforts and helps cement our belief that ecology, biophilia, commerce and real estate can co-exist at a single, iconic address.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/leed-gold.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-6">
                        <h2 class="NormalHeading">The philosophy of biophilia runs deep throughout the developments of Max Estates</h2>
                        <p>The natural world has inspired not just poets and philosophers through human history, but also scientists and entrepreneurs. It is only natural, then, that the place where you spend most of your day provides you with a connection to the natural world.</p>
                        <p> From the unique glass fitted into the façade to the very air you breathe inside Max House, everything you experience is designed to enhance the effects of nature on creativity, productivity and wellness. The three-tier air treatment technology delivers air quality on par with global standards; complete with CO2 sensors used to optimize ventilation and circulation.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/max-estate-22.png" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include('template-parts/footer.php') ?>