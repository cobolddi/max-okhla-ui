<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageHeader/InsideSliderBanner.php') ?>

<!-- Sidebar section -->
<section class="Section DesignPage">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="asideContainer">
                    <ul class="asideInner">
                        <li>
                            <a href="#Container-1" class="is_active">Location Highlights</a>
                        </li>
                        <li>
                            <a href="#Container-2">Easy Accessibility</a>
                        </li>
                        <li>
                            <a href="#Container-3">Branding and Visibility</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="NormalSection PTop12px">
                    <div class="NormalSectionContainer" id="Container-1">
                        <h2 class="NormalHeading">Location Highlights</h2>
                        <p>The site is well connected to key social infrastructure, transport options, and business districts.</p>
                        <div class="row innerRow">
                            <div class="col-6 col-md-4 firstCol">
                                <h4>Accessibility</h4>
                                <p>Excellent connectivity with the nearby Metro station and railway station</p>
                            </div>
                            <div class="col-6 col-md-4 secondCol">
                                <h4>Established Market</h4>
                                <p>One of the most prominent business-centric location in NCR</p>
                            </div>
                            <div class="col-6 col-md-4 thiredCol">
                                <h4>Ready Social Infrastructure</h4>
                                <p>Lotus Temple, F&B outlets, hospitals, hotels, etc</p>
                            </div>
                            <div class="col-6 col-md-4 thiredCol">
                                <h4>Rich Cultural Heritage</h4>
                                <p>Surrounded by heritage sites and boundless art history of Delhi.</p>
                            </div>
                            <div class="col-6 col-md-4 thiredCol">
                                <h4>Prime Residential Neighborhoods</h4>
                                <p>East of Kailash, Greater Kailash, and New Friends Colony</p>
                                
                            </div>
                            <div class="col-6 col-md-4 thiredCol">
                                <h4>Largest Captive Talent pool in NCR</h4>
                                <p>Largest Captive Talent pool in NCR</p>
                            </div>
                        </div>
                        <div class="imgWrap">
                            <!-- <div class="mapouter">
                                <div class="gmap_canvas">
                                    <iframe class="iframe" id="gmap_canvas" src="https://maps.google.com/maps?q=cobold&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    <a href="https://www.embedgooglemap.org">embed google map</a>
                                </div>
                            </div> -->
                            <img src="assets/img/tempimg/map.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-2">
                        <h2 class="NormalHeading">Easy Accessibility</h2>
                        <p><strong>Location:</strong> Most prime location within the Southern business district</p>
                        <p>Located at the intersection of three major roads</p>
                        <ul>
                            <li>Outer Ring Road (Connecting South & East Delhi)</li>
                            <li>Mathura Road (Connecting Mohan Coop. Estate and Central Delhi)</li>
                            <li>And Capt. Gaur Marg (Connecting Outer and inner ring roads)</li>
                        </ul>
                        <p>Within 200 meters of the Okhla NSIC metro station</p>
                        <p>Less than 1 Kilometre from the Kalkaji interchange (Purple line)</p>
                        <div class="imgWrap TwoImg">
                            <img src="assets/img/tempimg/easy-accessibility2.png" alt="">
                            <img src="assets/img/tempimg/easy-accessibility1.png" alt="">
                        </div>
                    </div>
                    <div class="NormalSectionContainer" id="Container-3">
                        <h2 class="NormalHeading">Branding and Visibility</h2>
                        <p>Max House will be the tallest development in Okhla Phase III which offers excellent visibility to the outer ring road, and the magenta line of the metro offering unparalleled visibility and brand recall.</p>
                        <p>Tenant would have the opportunity to take advantage of this unique premise and have their Corporate Identity visible on one of the most traveled stretches of NCR with average traffic volume exceeding 200,000 vehicles.</p>
                        <div class="imgWrap">
                            <img src="assets/img/tempimg/branding111.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="Section FourGridsWithBorder lightPinkSection">
    <div class="container SmallContainer">
        <div class="MultipleLogoSection">
            <div class="MainHeading">
                <h2>Lorem ipsum dolor sit amet</h2>
            </div>
            <ul class="BrandsLogo">
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
            </ul>
        </div>
        <ul class="GridsWithBorder">
            <li>
                <div class="GridsContaint">
                <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
        </ul>
    </div>
</section>

<!-- Footer form -->
<section class="Section FooterForm">
    <div class="container">
        <div class="FormBlockWithHeading CenterAlignForm">
            <h2>Register Your Interest</h2>
            <p>To arrange a call-back and schedule a visit to our experience center, please submit<br> your contact details:</p>
            <form action="">

                <div class="row">                
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="First Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Last Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="email" placeholder="Email"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Phone"></div>
                    <div class="col-12 col-md-12"><textarea class="button draw" placeholder="Mesaage"></textarea></div>
                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
                </div>      
                
            </form>
        </div>
    </div>
</section>



<?php @include('template-parts/footer.php') ?>