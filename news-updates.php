<?php @include('template-parts/header.php') ?>
<div class="newsUpdatesWrapper">
<!-- Page banner -->
	
	<section class="NewsUpdateBanner">
		<div class="container">
			<div class="NewsBannerContent">
				<div class="HeadingContent">
					<h2>News & Updates</h2>
					<p>Max House is dedicated to supporting people and business with a variety of environments and<br> services enhances working life and productivity.</p>
				</div>
				<div class="ImageContent">
					<div class="row">
						<div class="col-12 col-md-5">
							<div class="BannerContent">
								<h3>Open for Business soon!<br> In the heart of the<br> New Delhi</h3>
							</div>
						</div>
						<div class="col-12 col-md-7">
							<img src="assets/img/tempimg/newsUpdateBanner.png" alt="" class="DesktopBanner">
						</div>
						<!-- <div class="DesktopBanner DesktopOnly">
							<img src="assets/img/tempimg/newsUpdateBanner.png" alt="">
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Card Section -->
	<section class="topHeadingBottomInfoCard Section">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-sm-6 MBottom3">
					<div class="innerCard">
						<span>May 05, 2019</span>
						<h3>Sahil Vachani: Building Max Group's real estate dreams</h3>
						<p>The first thing one notices while entering Max Towers is the statue in the courtyard. It is of a man doing a version of halasana, the plow pose in Yoga…</p>
						<a href="#" class="BtnWithBorder MaroonBorder MTop0">Read More</a>
					</div>
				</div>
				
				<div class="col-lg-4 col-sm-6 MBottom3">
					<div class="innerCard">
						<span>April 24, 2019</span>
						<h3>A new blueprint for working well</h3>
						<p>A gigantic upside-down man assembled with uneven stone grabs your eye as you walk into the reception area of Max Towers in Noida. This massive sculpture, gorgeously crafted by South African…</p>
						<a href="#" class="BtnWithBorder MaroonBorder MTop0">Read More</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6 MBottom3">
					<div class="innerCard">
						<span>April 12, 2019</span>
						<h3>Max Group enters commercial real estate with Max Towers’ opening on DND</h3>
						<p>Max Estates, a subsidiary of Max-Ventures & Industries Ltd. (MaxVIL)…</p>
						<a href="#" class="BtnWithBorder MaroonBorder MTop0">Read More</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6 MBottom3">
					<div class="innerCard">
						<span>June 05, 2018</span>
						<h3>Sahil Vachani: Building Max Group's real estate dreams</h3>
						<p>The first thing one notices while entering Max Towers is the statue in the courtyard. It is of a man doing a version of halasana, the plow pose in Yoga…</p>
						<a href="#" class="BtnWithBorder MaroonBorder MTop0">Read More</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6 MBottom3">
					<div class="innerCard">
						<span>June 05, 2018</span>
						<h3>Sahil Vachani: Building Max Group's real estate dreams</h3>
						<p>The first thing one notices while entering Max Towers is the statue in the courtyard. It is of a man doing a version of halasana, the plow pose in Yoga…</p>
						<a href="#" class="BtnWithBorder MaroonBorder MTop0">Read More</a>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6 MBottom3">
					<div class="innerCard">
						<span>June 05, 2018</span>
						<h3>Sahil Vachani: Building Max Group's real estate dreams</h3>
						<p>The first thing one notices while entering Max Towers is the statue in the courtyard. It is of a man doing a version of halasana, the plow pose in Yoga…</p>
						<a href="#" class="BtnWithBorder MaroonBorder MTop0">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Four Grid -->
	<section class="Section FourGridsWithBorder lightPinkSection">
	    <div class="container SmallContainer">
	        <div class="MultipleLogoSection">
	            <div class="MainHeading">
	                <h2>Lorem ipsum dolor sit amet</h2>
	            </div>
	            <ul class="BrandsLogo">
	                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
	                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
	                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
	            </ul>
	        </div>
	        <ul class="GridsWithBorder">
	            <li>
	                <div class="GridsContaint">
	                    <h4>Lorem ipsum dolor sit amet</h4>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar. posuere fermentum sapien. Donec vel ante lorem. posuere fermentum sapien. Donec vel ante lorem. posuere fermentum sapien. Donec vel ante lorem.</p>
	                </div>
	            </li>
	            <li>
	                <div class="GridsContaint">
	                    <h4>Lorem ipsum dolor sit amet</h4>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
	                </div>
	            </li>
	            <li>
	                <div class="GridsContaint">
	                    <h4>Lorem ipsum dolor sit amet</h4>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
	                </div>
	            </li>
	            <li>
	                <div class="GridsContaint">
	                    <h4>Lorem ipsum dolor sit amet</h4>
	                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
	                </div>
	            </li>
	        </ul>
	    </div>
	</section>

</div>
<?php @include('template-parts/footer.php') ?>
