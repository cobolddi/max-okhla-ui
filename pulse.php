<?php @include('template-parts/header.php') ?>

<!-- Pulse heading   -->
<section class="PulseHeadingSection">
    <div class="container">
        <div class="pulseHeading">
            <img src="assets/img/PulseLogo2.svg" alt="">
             <p>Max House is a building that literally pulses with life. Our intention is to blend thoughtful community experiences, conveniences, technology and design elements with superior hospitality to create an environment, which integrates work and life. We deliver the “WorkWell” promise through the concept of Pulse by bringing life to a brick-and-mortar building and nurturing the occupants within to be more productive, healthier and happier.</p>
        </div>

    </div>
</section>

<!-- Page header -->
<section class="SliderBanner">
    <div class="container">
        <div class="InsideBanner">
            <div>
                <img src="assets/img/Design.png" class="DesktopOnly" alt="">
                <img src="assets/img/DesignMobile.png" class="MobileOnly" alt="">
            </div>
            <div>
                <img src="assets/img/Design.png" class="DesktopOnly" alt="">
                <img src="assets/img/DesignMobile.png" class="MobileOnly" alt="">
            </div>
            <div>
                <img src="assets/img/Design.png" class="DesktopOnly" alt="">
                <img src="assets/img/DesignMobile.png" class="MobileOnly" alt="">
            </div>
        </div>
    </div>
</section>

<!-- Sidebar section -->
<section class="Section DesignPage">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="asideContainer">
                    <ul class="asideInner">
                        <li>
                            <a href="#Container-1" class="is_active">About Pulse</a>
                        </li>
                        <li>
                            <a href="#Container-2">Gallery</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="NormalSection PTop12px">
                    <div class="NormalSectionContainer" id="Container-1">
                        <h2 class="NormalHeading">About Pulse</h2>
                        <!-- Pulse container -->
                        <div class="PulseCardContainer">
                            <div class="row" style="">
                                <div class="col-12 col-lg-6 SinglePulseCard PulseGreen">
                                    <a href="#PulseCommmunity" data-id="PulseCommmunity">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-community"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Community</div>
                                        </div>
                                    </a>
                                </div>
                            
                                <div class="col-12 col-lg-6 SinglePulseCard PulsePurple">
                                    <a href="#PulseConveniences" data-id="PulseConveniences">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-plantHand"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Conveniences</div>
                                        </div>
                                    </a>
                                </div>
                            
                                <div class="col-12 col-lg-6 SinglePulseCard PulseYellow">
                                    <a href="#PulseTechnology" data-id="PulseTechnology">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-technology"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Technology</div>
                                        </div>
                                    </a>
                                </div>
                            
                                <div class="col-12 col-lg-6 SinglePulseCard PulsePink">
                                    <a href="#PulseDesignBuild" data-id="PulseDesignBuild">
                                        <div class="PulseCard">
                                            <div class="Image">
                                                <div class="IconWrap">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-DesignBuild"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="Head">Design &amp; Build</div>
                                        </div>
                                    </a>
                                </div>
                                    
                            </div>

                            <div class="PulseCardContent PulseGreen" id="PulseCommmunity" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent PulseGreen">
                                    <div class="Image">
                                        <!-- <img src="https://www.maxtowers.com/wp-content/uploads/2019/03/Community.png" alt=""> -->
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-community"></use>
                                            </svg>      
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Community</span> - Enriching your lives through
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>Curation and execution of a monthly calendar of activities at Max Towers.</li>
                                        <li>Events on weekdays and weekends across a variety of genres that allow you the flexibility to pick and choose basis your convenience and interest.</li>
                                        <li>Easy access to casual and fine dining outlets for a work or personal get together or celebration.</li>
                                        <li>Preferred use of spaces and access to activities and initiatives on site.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="PulseCardContent PulsePurple" id="PulseConveniences" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent">
                                    <div class="Image">
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-plantHand"></use>
                                            </svg>       
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Conveniences</span> - Simplifying your lives by
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>Drawing the balance between work and life more favorably by moving your weekend chores on-site e.g. car service, salon visits, etc.</li>
                                        <li>Enabling you to get more done in life through easy access to weekday routine activities like workouts, swimming, groceries, laundry, etc.</li>
                                        <li>Providing a helping and caring hand to you through health checkups, pharmacy access, etc.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="PulseCardContent PulseYellow" id="PulseTechnology" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent">
                                    <div class="Image">
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-technology"></use>
                                            </svg>     
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Technology</span> - Enabling&nbsp;you to WorkWell&nbsp;through
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>A building app that connects you to others systems in the building – smart access, event ticketing, online food ordering, visitor management, meeting room booking, etc.</li>
                                        <li>An integrated visitor management system that allows you to seamlessly manage visitor movement.</li>
                                        <li>Meeting room booking system that enables you to efficiently manage meeting spaces.</li>
                                        <li>An intuitive building management system that drives efficiency and optimization in utilities.</li>
                                    </ol>
                                </div>
                            </div>

                            <div class="PulseCardContent PulsePink" id="PulseDesignBuild" style="display: none;">
                                <a href="#" class="ClosePulseCardContent"></a>
                                <div class="HeaderContent">
                                    <div class="Image">
                                        <div class="IconWrap">
                                            <svg class="Icon">
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-DesignBuild"></use>
                                            </svg>     
                                        </div>
                                    </div>
                                    <div class="Head">
                                        <span class="Title">Design &amp; Build</span> - Bringing&nbsp;in depth awareness around impact of design and build in your lives through
                                    </div>
                                </div>
                                <div class="MainContent">
                                    <ol>
                                        <li>Increased biophilia&nbsp;throughout the&nbsp;building&nbsp;including your floor.</li>
                                        <li>Refresh your workspace aspects to create or reinforce your organization’s cultural ethos.</li>
                                        <li>Contemporary design incorporating modern workplace trends suited to your needs through our preferred award-winning design partners.</li>
                                        <li>Range of build-out services suiting your budget through our preferred fit-out partners.</li>
                                        <li>Incorporation of subtle elements that induce employee wellness.</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <!-- End pulse container -->
                    </div>
                    <div class="NormalSectionContainer" id="Container-2">
                        <h2 class="NormalHeading">Gallery</h2>
                        <div class="ImageGallerySection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-md-6 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                            <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-6 col-md-6">
                                                <div class="row ImageGalleryInnerRow">
                                                    <div class="col-md-12 MBottom2">
                                                        <div class="ImageGalleryBox">
                                                            <a href="assets/img/tempimg/gallery-3.png" class="ImageGalleryPopup">
                                                                <div class="ImageGalleryOverlay">
                                                                    <svg class="Icon">
                                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                                    </svg>
                                                                </div>
                                                                <img src="assets/img/tempimg/gallery-3.png" alt="Image">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 MBottom2">
                                                        <div class="ImageGalleryBox">
                                                            <a href="assets/img/tempimg/gallery-3.png" class="ImageGalleryPopup">
                                                                <div class="ImageGalleryOverlay">
                                                                    <svg class="Icon">
                                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                                    </svg>
                                                                </div>
                                                                <img src="assets/img/tempimg/gallery-3.png" alt="Image">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-md-6">
                                                <div class="ImageGalleryBox">
                                                    <a href="assets/img/tempimg/gallery-4.png" class="ImageGalleryPopup">
                                                        <div class="ImageGalleryOverlay">
                                                            <svg class="Icon">
                                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                            </svg>
                                                        </div>
                                                        <img src="assets/img/tempimg/gallery-4.png" alt="Image">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-5.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                                <img src="assets/img/tempimg/gallery-5.png" alt="Image">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-6.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                                <img src="assets/img/tempimg/gallery-6.png" alt="Image">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-7.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                                <img src="assets/img/tempimg/gallery-7.png" alt="Image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row LoadMoreImageRow">
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                            <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                                        </div>
                                    </div>
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                            <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                                        </div>
                                    </div>
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                            <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                                        </div>
                                    </div>
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                            <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                                        </div>
                                    </div>
                                    <div class="col-4 col-md-4 MBottom2">
                                        <div class="ImageGalleryBox">
                                            <a href="assets/img/tempimg/gallery-1.png" class="ImageGalleryPopup">
                                                <div class="ImageGalleryOverlay">
                                                    <svg class="Icon">
                                                        <use xlink:href="assets/img/cobold-sprite.svg#icon-image_Zoom"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                            <img src="assets/img/tempimg/gallery-1.png" alt="Image">
                                        </div>
                                    </div>
                                </div>
                                <div class="BtnWrap">
                                    <a href="#" class="Button BtnWithBorder MaroonBorder">Load more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Four Grid -->
<section class="Section FourGridsWithBorder lightPinkSection">
    <div class="container SmallContainer">
        <div class="MultipleLogoSection">
            <div class="MainHeading">
                <h2>Lorem ipsum dolor sit amet</h2>
            </div>
            <ul class="BrandsLogo">
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
            </ul>
        </div>
        <ul class="GridsWithBorder">
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar. posuere fermentum sapien. Donec vel ante lorem. posuere fermentum sapien. Donec vel ante lorem. posuere fermentum sapien. Donec vel ante lorem.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
        </ul>
    </div>
</section>


<!-- Footer form -->
<section class="Section FooterForm">
    <div class="container">
        <div class="FormBlockWithHeading CenterAlignForm">
            <h2>Register Your Interest</h2>
            <p>To arrange a call-back and schedule a visit to our experience center, please submit<br> your contact details:</p>
            <form action="">

                <div class="row">                
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="First Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Last Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="email" placeholder="Email"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Phone"></div>
                    <div class="col-12 col-md-12"><textarea class="button draw" placeholder="Mesaage"></textarea></div>
                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
                </div>      
                
            </form>
        </div>
    </div>
</section>



<?php @include('template-parts/footer.php') ?>