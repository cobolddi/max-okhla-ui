

<?php @include('template-parts/header.php') ?>

<!-- Contact form -->
<section class="Section ContactUsForm">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 MBottom3">
				<div class="RegisterYourself">
					<div class="MainHeading">
						<h2>Register Your Interest</h2>
						<p>To arrange a call-back and schedule a visit to our experience center,<br> please submit your contact details:</p>
					</div>
					<div class="FormSection">
						<form>
			                <div class="row">                
			                    <div class="col-12 col-md-6"><input type="text" placeholder="First Name"></div>
			                    <div class="col-12 col-md-6"><input type="text" placeholder="Last Name"></div>
			                    <div class="col-12 col-md-6"><input type="email" placeholder="Email"></div>
			                    <div class="col-12 col-md-6"><input type="text" placeholder="Phone"></div>
			                    <div class="col-12 col-md-12"><textarea placeholder="Mesaage"></textarea></div>
			                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
			                </div>                  
			            </form>
		            </div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="ContactInfo">
					<h3>Contact Information</h3>
					<ul class="details">
						<li>
							<h4>Max Okhla</h4>
							<p>Okhla Industrial Area, Phase 3, New Delhi - 110020</p>
						</li>
						<li>
							<h4>E-Mail Id</h4>
							<p><a href="mailto:info@maxhouseokhla.in">info@maxhouseokhla.in</a></p>
						</li>
						<li>
							<h4>Phone Number</h4>
							<p><a href="tel:+91-955539522">+91-955539522</a></p>
						</li>
						<li>
							<h4>Follow Us On</h4>
							<ul class="SocialButton">
	                            <li>
	                                <a href="#"><img src="assets/img/twitter.svg" alt=""></a>
	                            </li>
	                            <li>
	                                <a href="#"><img src="assets/img/linkedin.svg" alt=""></a>
	                            </li>
	                        </ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Four grid -->
<section class="Section FourGridsWithBorder lightPinkSection">
    <div class="container SmallContainer">
        <div class="MultipleLogoSection">
            <div class="MainHeading">
                <h2>Lorem ipsum dolor sit amet</h2>
            </div>
            <ul class="BrandsLogo">
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
                <li><a href="#"><img src="assets/img/maxestateslogo.png" alt=""></a></li>
            </ul>
        </div>
        <ul class="GridsWithBorder">
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar. posuere fermentum sapien. Donec vel ante lorem. posuere fermentum sapien. Donec vel ante lorem. posuere fermentum sapien. Donec vel ante lorem.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
            <li>
                <div class="GridsContaint">
                    <h4>Lorem ipsum dolor sit amet</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc orci turpis, interdum vel tortor id, posuere fermentum sapien. Donec vel ante lorem. Nam cursus aliquet pulvinar.</p>
                </div>
            </li>
        </ul>
    </div>
</section>

<section class="Section FooterForm">
    <div class="container">
        <div class="FormBlockWithHeading CenterAlignForm">
            <h2>Register Your Interest</h2>
            <p>To arrange a call-back and schedule a visit to our experience center, please submit<br> your contact details:</p>
            <form action="">

                <div class="row">                
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="First Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Last Name"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="email" placeholder="Email"></div>
                    <div class="col-12 col-md-6"><input class="button draw" type="text" placeholder="Phone"></div>
                    <div class="col-12 col-md-12"><textarea class="button draw" placeholder="Mesaage"></textarea></div>
                    <div class="col-12 col-md-12"><input type="submit" value="Send Message"></div>
                </div>      
                
            </form>
        </div>
    </div>
</section>

<?php @include('template-parts/footer.php') ?>